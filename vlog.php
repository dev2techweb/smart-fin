<?php
/*
* Template Name: vlog
*/
get_header()
?>
  <section class="form__template w-50 e-center t-center page top">
    <h1 class="title t-black t-center title__page"><?php the_title(); ?></h1>
    <div class="form__section">
      <?php if (have_posts()): while (have_posts()) : the_post(); ?>
        <?php the_content(); ?>
      <?php endwhile; endif; ?>
    </div>
  </section>
